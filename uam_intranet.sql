-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-10-2017 a las 23:54:35
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `uam_intranet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'News', 'news', '2017-10-03 23:14:58', '2017-10-04 02:14:36'),
(3, NULL, 1, 'Tutorials', 'tutorials', '2017-10-04 02:16:09', '2017-10-04 02:16:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(3, 1, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(4, 1, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(5, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(6, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(7, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(8, 1, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 8),
(9, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 9),
(10, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 10),
(11, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(12, 1, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 12),
(13, 1, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 13),
(14, 2, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(15, 2, 'author_id', 'text', 'author_id', 1, 0, 0, 0, 0, 0, '', 2),
(16, 2, 'title', 'text', 'title', 1, 1, 1, 1, 1, 1, '', 3),
(17, 2, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(18, 2, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 5),
(19, 2, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 6),
(20, 2, 'meta_description', 'text', 'meta_description', 1, 0, 1, 1, 1, 1, '', 7),
(21, 2, 'meta_keywords', 'text', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 8),
(22, 2, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(23, 2, 'created_at', 'timestamp', 'created_at', 1, 1, 1, 0, 0, 0, '', 10),
(24, 2, 'updated_at', 'timestamp', 'updated_at', 1, 0, 0, 0, 0, 0, '', 11),
(25, 2, 'image', 'image', 'image', 0, 1, 1, 1, 1, 1, '', 12),
(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, NULL, 2),
(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, NULL, 3),
(29, 3, 'password', 'password', 'password', 1, 0, 0, 1, 1, 0, NULL, 4),
(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 0, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(31, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, NULL, 5),
(32, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, NULL, 6),
(33, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, NULL, 7),
(34, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(35, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(36, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(37, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(38, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(39, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(40, 4, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(41, 4, 'order', 'text', 'order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(42, 4, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 4),
(43, 4, 'slug', 'text', 'slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(44, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 1, 0, 0, 0, '', 6),
(45, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
(46, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(48, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(49, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(51, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 14),
(52, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 15),
(53, 3, 'role_id', 'hidden', 'role_id', 0, 1, 1, 1, 1, 1, NULL, 9),
(69, 10, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(70, 10, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(71, 10, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, NULL, 3),
(72, 10, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, NULL, 4),
(73, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(74, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(75, 11, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(76, 11, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(77, 11, 'body', 'text_area', 'Body', 0, 0, 1, 1, 1, 1, NULL, 3),
(78, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 4),
(79, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(84, 13, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(85, 13, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(86, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(87, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(88, 3, 'user_belongsto_team_relationship', 'relationship', 'teams', 0, 1, 1, 1, 1, 0, '{\"model\":\"App\\\\Team\",\"table\":\"teams\",\"type\":\"belongsTo\",\"column\":\"team_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 11),
(89, 3, 'team_id', 'checkbox', 'Team Id', 0, 1, 1, 1, 1, 1, NULL, 10),
(90, 14, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(91, 14, 'user_id', 'number', 'User Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(92, 14, 'mothn', 'date', 'Mothn', 0, 1, 1, 1, 1, 1, NULL, 3),
(93, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(94, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(95, 14, 'employee_month_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 6),
(96, 16, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(97, 16, 'user_id', 'hidden', 'User Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(98, 16, 'month', 'date', 'Month', 0, 1, 1, 1, 1, 1, NULL, 3),
(99, 16, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 4),
(100, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(101, 16, 'employee_month_belongsto_user_relationship_1', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 6),
(116, 19, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(117, 19, 'id_value', 'hidden', 'Id Value', 0, 0, 1, 1, 1, 1, NULL, 2),
(118, 19, 'id_team', 'hidden', 'Id Team', 0, 0, 1, 1, 1, 1, NULL, 3),
(119, 19, 'date', 'date', 'Date', 0, 1, 1, 1, 1, 1, NULL, 4),
(120, 19, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 5),
(121, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(122, 19, 'weekvalue_belongsto_uamvalue_relationship', 'relationship', 'uamvalues', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Uamvalue\",\"table\":\"uamvalues\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 7),
(123, 19, 'weekvalue_belongsto_team_relationship', 'relationship', 'teams', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Team\",\"table\":\"teams\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
(1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, '2017-10-03 23:14:48', '2017-10-03 23:14:48'),
(2, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, '2017-10-03 23:14:48', '2017-10-03 23:14:48'),
(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '2017-10-03 23:14:48', '2017-10-05 00:31:41'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, '2017-10-03 23:14:48', '2017-10-03 23:14:48'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, '2017-10-03 23:14:48', '2017-10-03 23:14:48'),
(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, '2017-10-03 23:14:48', '2017-10-03 23:14:48'),
(10, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '2017-10-04 03:45:38', '2017-10-04 03:45:38'),
(11, 'uamvalues', 'uamvalues', 'Uamvalue', 'Uamvalues', 'icon voyager-certificate', 'App\\Uamvalue', NULL, NULL, NULL, 1, 0, '2017-10-04 03:49:44', '2017-10-05 19:55:39'),
(13, 'teams', 'teams', 'Team', 'Teams', NULL, 'App\\Team', NULL, NULL, NULL, 1, 0, '2017-10-04 23:49:11', '2017-10-04 23:49:11'),
(14, 'employee_month', 'employee-month', 'Employee of the month', 'Employees of the month', NULL, 'App\\EmployeeMonth', NULL, NULL, NULL, 1, 0, '2017-10-05 02:36:02', '2017-10-05 02:36:02'),
(16, 'employee_months', 'employee-months', 'Employee of the Month', 'Employees of the Month', 'icon voyager-trophy', 'App\\EmployeeMonth', NULL, NULL, NULL, 1, 0, '2017-10-05 02:49:40', '2017-10-05 19:56:19'),
(19, 'weekvalues', 'weekvalues', 'Weekvalue', 'Weekvalues', 'voyager-window-list', 'App\\Weekvalue', NULL, NULL, NULL, 1, 0, '2017-10-06 23:32:28', '2017-10-06 23:46:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee_months`
--

CREATE TABLE `employee_months` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `month` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `employee_months`
--

INSERT INTO `employee_months` (`id`, `user_id`, `month`, `created_at`, `updated_at`) VALUES
(1, 2, '2017-10-04', '2017-10-05 02:54:41', '2017-10-05 02:54:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2017-10-03 23:14:52', '2017-10-03 23:14:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-calendar', '#000000', NULL, 1, '2017-10-03 23:14:52', '2017-10-05 02:10:20', 'voyager.dashboard', 'null'),
(2, 1, 'Files', '', '_self', 'voyager-archive', '#000000', NULL, 4, '2017-10-03 23:14:52', '2017-10-05 02:11:51', 'voyager.media.index', 'null'),
(3, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2017-10-03 23:14:52', '2017-10-04 03:38:27', 'voyager.posts.index', NULL),
(4, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2017-10-03 23:14:52', '2017-10-03 23:14:52', 'voyager.users.index', NULL),
(6, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2017-10-03 23:14:52', '2017-10-04 03:38:27', 'voyager.pages.index', NULL),
(7, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2017-10-03 23:14:52', '2017-10-03 23:14:52', 'voyager.roles.index', NULL),
(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2017-10-03 23:14:52', '2017-10-05 03:18:50', NULL, NULL),
(9, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 8, 1, '2017-10-03 23:14:52', '2017-10-04 03:38:27', 'voyager.menus.index', NULL),
(10, 1, 'Database', '', '_self', 'voyager-data', NULL, 8, 2, '2017-10-03 23:14:53', '2017-10-04 03:38:27', 'voyager.database.index', NULL),
(11, 1, 'Compass', '/admin/compass', '_self', 'voyager-compass', NULL, 8, 3, '2017-10-03 23:14:53', '2017-10-04 03:38:27', NULL, NULL),
(12, 1, 'Hooks', '/admin/hooks', '_self', 'voyager-hook', NULL, 8, 4, '2017-10-03 23:14:53', '2017-10-04 03:38:27', NULL, NULL),
(13, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2017-10-03 23:14:53', '2017-10-05 03:18:50', 'voyager.settings.index', NULL),
(14, 1, 'Values', '', '_self', 'voyager-certificate', '#000000', NULL, 8, '2017-10-04 03:38:19', '2017-10-05 02:23:24', 'voyager.uamvalues.index', 'null'),
(15, 1, 'Employee of the month', '', '_self', 'voyager-trophy', '#000000', NULL, 9, '2017-10-05 03:18:39', '2017-10-05 03:19:26', 'voyager.employee-months.index', 'null');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
(17, '2017_01_15_000000_create_permission_groups_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(21, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(22, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(23, '2017_08_05_000000_add_group_to_settings_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2017-10-03 23:14:59', '2017-10-03 23:14:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2017-10-03 23:14:53', '2017-10-03 23:14:53', NULL),
(2, 'browse_database', NULL, '2017-10-03 23:14:53', '2017-10-03 23:14:53', NULL),
(3, 'browse_media', NULL, '2017-10-03 23:14:53', '2017-10-03 23:14:53', NULL),
(4, 'browse_compass', NULL, '2017-10-03 23:14:53', '2017-10-03 23:14:53', NULL),
(5, 'browse_menus', 'menus', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(6, 'read_menus', 'menus', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(7, 'edit_menus', 'menus', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(8, 'add_menus', 'menus', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(9, 'delete_menus', 'menus', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(10, 'browse_pages', 'pages', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(11, 'read_pages', 'pages', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(12, 'edit_pages', 'pages', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(13, 'add_pages', 'pages', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(14, 'delete_pages', 'pages', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(15, 'browse_roles', 'roles', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(16, 'read_roles', 'roles', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(17, 'edit_roles', 'roles', '2017-10-03 23:14:54', '2017-10-03 23:14:54', NULL),
(18, 'add_roles', 'roles', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(19, 'delete_roles', 'roles', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(20, 'browse_users', 'users', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(21, 'read_users', 'users', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(22, 'edit_users', 'users', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(23, 'add_users', 'users', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(24, 'delete_users', 'users', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(25, 'browse_posts', 'posts', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(26, 'read_posts', 'posts', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(27, 'edit_posts', 'posts', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(28, 'add_posts', 'posts', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(29, 'delete_posts', 'posts', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(30, 'browse_categories', 'categories', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(31, 'read_categories', 'categories', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(32, 'edit_categories', 'categories', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(33, 'add_categories', 'categories', '2017-10-03 23:14:55', '2017-10-03 23:14:55', NULL),
(34, 'delete_categories', 'categories', '2017-10-03 23:14:56', '2017-10-03 23:14:56', NULL),
(35, 'browse_settings', 'settings', '2017-10-03 23:14:56', '2017-10-03 23:14:56', NULL),
(36, 'read_settings', 'settings', '2017-10-03 23:14:56', '2017-10-03 23:14:56', NULL),
(37, 'edit_settings', 'settings', '2017-10-03 23:14:56', '2017-10-03 23:14:56', NULL),
(38, 'add_settings', 'settings', '2017-10-03 23:14:56', '2017-10-03 23:14:56', NULL),
(39, 'delete_settings', 'settings', '2017-10-03 23:14:56', '2017-10-03 23:14:56', NULL),
(55, 'browse_products', 'products', '2017-10-04 03:45:39', '2017-10-04 03:45:39', NULL),
(56, 'read_products', 'products', '2017-10-04 03:45:39', '2017-10-04 03:45:39', NULL),
(57, 'edit_products', 'products', '2017-10-04 03:45:39', '2017-10-04 03:45:39', NULL),
(58, 'add_products', 'products', '2017-10-04 03:45:39', '2017-10-04 03:45:39', NULL),
(59, 'delete_products', 'products', '2017-10-04 03:45:39', '2017-10-04 03:45:39', NULL),
(60, 'browse_uamvalues', 'uamvalues', '2017-10-04 03:49:44', '2017-10-04 03:49:44', NULL),
(61, 'read_uamvalues', 'uamvalues', '2017-10-04 03:49:44', '2017-10-04 03:49:44', NULL),
(62, 'edit_uamvalues', 'uamvalues', '2017-10-04 03:49:44', '2017-10-04 03:49:44', NULL),
(63, 'add_uamvalues', 'uamvalues', '2017-10-04 03:49:44', '2017-10-04 03:49:44', NULL),
(64, 'delete_uamvalues', 'uamvalues', '2017-10-04 03:49:44', '2017-10-04 03:49:44', NULL),
(70, 'browse_teams', 'teams', '2017-10-04 23:49:11', '2017-10-04 23:49:11', NULL),
(71, 'read_teams', 'teams', '2017-10-04 23:49:11', '2017-10-04 23:49:11', NULL),
(72, 'edit_teams', 'teams', '2017-10-04 23:49:11', '2017-10-04 23:49:11', NULL),
(73, 'add_teams', 'teams', '2017-10-04 23:49:11', '2017-10-04 23:49:11', NULL),
(74, 'delete_teams', 'teams', '2017-10-04 23:49:11', '2017-10-04 23:49:11', NULL),
(75, 'browse_employee_month', 'employee_month', '2017-10-05 02:36:02', '2017-10-05 02:36:02', NULL),
(76, 'read_employee_month', 'employee_month', '2017-10-05 02:36:02', '2017-10-05 02:36:02', NULL),
(77, 'edit_employee_month', 'employee_month', '2017-10-05 02:36:02', '2017-10-05 02:36:02', NULL),
(78, 'add_employee_month', 'employee_month', '2017-10-05 02:36:02', '2017-10-05 02:36:02', NULL),
(79, 'delete_employee_month', 'employee_month', '2017-10-05 02:36:02', '2017-10-05 02:36:02', NULL),
(80, 'browse_employee_months', 'employee_months', '2017-10-05 02:49:40', '2017-10-05 02:49:40', NULL),
(81, 'read_employee_months', 'employee_months', '2017-10-05 02:49:40', '2017-10-05 02:49:40', NULL),
(82, 'edit_employee_months', 'employee_months', '2017-10-05 02:49:40', '2017-10-05 02:49:40', NULL),
(83, 'add_employee_months', 'employee_months', '2017-10-05 02:49:40', '2017-10-05 02:49:40', NULL),
(84, 'delete_employee_months', 'employee_months', '2017-10-05 02:49:40', '2017-10-05 02:49:40', NULL),
(95, 'browse_weekvalues', 'weekvalues', '2017-10-06 23:32:28', '2017-10-06 23:32:28', NULL),
(96, 'read_weekvalues', 'weekvalues', '2017-10-06 23:32:28', '2017-10-06 23:32:28', NULL),
(97, 'edit_weekvalues', 'weekvalues', '2017-10-06 23:32:28', '2017-10-06 23:32:28', NULL),
(98, 'add_weekvalues', 'weekvalues', '2017-10-06 23:32:28', '2017-10-06 23:32:28', NULL),
(99, 'delete_weekvalues', 'weekvalues', '2017-10-06 23:32:28', '2017-10-06 23:32:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(3, 3),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(20, 3),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(23, 3),
(24, 1),
(24, 3),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-10-03 23:14:59', '2017-10-07 03:34:55'),
(2, 1, 1, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n<h2>We can use all kinds of format!</h2>\r\n<p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-10-03 23:14:59', '2017-10-07 03:35:13'),
(3, 1, 3, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-10-03 23:14:59', '2017-10-07 03:35:26'),
(4, 1, 3, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-10-03 23:14:59', '2017-10-07 03:35:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2017-10-03 23:14:53', '2017-10-03 23:14:53'),
(2, 'user', 'Normal User', '2017-10-03 23:14:53', '2017-10-03 23:14:53'),
(3, 'coach', 'coach', '2017-10-04 20:41:37', '2017-10-04 20:41:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'UAM', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Intranet', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings/October2017/logoUam1.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/October2017/logoUam.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `teams`
--

INSERT INTO `teams` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Programming and Design Team', '2017-10-04 23:52:00', '2017-10-05 00:49:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 1, 'pt', 'Post', '2017-10-03 23:15:00', '2017-10-03 23:15:00'),
(2, 'data_types', 'display_name_singular', 2, 'pt', 'Página', '2017-10-03 23:15:00', '2017-10-03 23:15:00'),
(3, 'data_types', 'display_name_singular', 3, 'pt', 'Utilizador', '2017-10-03 23:15:00', '2017-10-03 23:15:00'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(5, 'data_types', 'display_name_singular', 5, 'pt', 'Menu', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(6, 'data_types', 'display_name_singular', 6, 'pt', 'Função', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(7, 'data_types', 'display_name_plural', 1, 'pt', 'Posts', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(8, 'data_types', 'display_name_plural', 2, 'pt', 'Páginas', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(9, 'data_types', 'display_name_plural', 3, 'pt', 'Utilizadores', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(11, 'data_types', 'display_name_plural', 5, 'pt', 'Menus', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(12, 'data_types', 'display_name_plural', 6, 'pt', 'Funções', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2017-10-03 23:15:01', '2017-10-03 23:15:01'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(22, 'menu_items', 'title', 3, 'pt', 'Publicações', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(23, 'menu_items', 'title', 4, 'pt', 'Utilizadores', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(24, 'menu_items', 'title', 5, 'pt', 'Categorias', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(25, 'menu_items', 'title', 6, 'pt', 'Páginas', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(26, 'menu_items', 'title', 7, 'pt', 'Funções', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(27, 'menu_items', 'title', 8, 'pt', 'Ferramentas', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(28, 'menu_items', 'title', 9, 'pt', 'Menus', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(29, 'menu_items', 'title', 10, 'pt', 'Base de dados', '2017-10-03 23:15:02', '2017-10-03 23:15:02'),
(30, 'menu_items', 'title', 13, 'pt', 'Configurações', '2017-10-03 23:15:02', '2017-10-03 23:15:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uamvalues`
--

CREATE TABLE `uamvalues` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `uamvalues`
--

INSERT INTO `uamvalues` (`id`, `name`, `body`, `created_at`, `updated_at`) VALUES
(1, 'CUSTOMER FOCUS', 'We care for our clients and look to become an integral part of their businesses.', '2017-10-04 22:33:13', '2017-10-04 22:33:13'),
(2, 'RESPECT', 'We respect our peers, clients, and providers.', '2017-10-04 22:34:55', '2017-10-04 22:34:55'),
(3, 'TEAMWORK', 'We understand that we as a team are stronger than each member individually', '2017-10-04 22:35:57', '2017-10-04 22:35:57'),
(4, 'SENSE OF URGENCY', 'We act with the realization that efficiency is vital to success. Our clients’ priorities become ur priorities', '2017-10-04 22:36:15', '2017-10-04 22:36:15'),
(6, 'PUNCTUALITY', 'We arrive on time and deliver clients’ tasks on time', '2017-10-04 22:36:43', '2017-10-04 22:36:43'),
(7, 'QUALITY IN EVERYTHING WE DO', 'We do our best in all our actions', '2017-10-04 22:37:30', '2017-10-04 22:37:30'),
(8, 'DISCIPLINE', 'We follow all rules and guidelines', '2017-10-04 22:38:13', '2017-10-04 22:38:13'),
(9, 'RESULT DRIVEN', 'We strive for efficiency and effectiveness Commitment. We put dedication in all our activities and look for the overall well-being of the company', '2017-10-04 22:38:33', '2017-10-05 20:20:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`, `team_id`) VALUES
(1, 1, 'Giovanni Rodriguez', 'giovannir@uassist.me', 'users/October2017/Gio.jpg', '$2y$10$uzvDAz5QUJgIwMrmjgw6Be/8.afmBtz6UokYIMfjEZ/3govI/.Swy', 'wgt612iWbiinNusRuptaQOD7Tne2TbcHSk1JlK21TnI9tBD9ZP3eknS0F0Yh', '2017-10-03 23:14:58', '2017-10-07 01:15:16', 1),
(2, 2, 'John Doe', 'johndoe@uassist.me', 'users/default.png', '$2y$10$FRytzGJYaW3qAPIa7OnPc.NagyRLW.WsusZnGw0YfSwr1c1LhgWqS', 'O0G6GLcY4Nn9NjE99fL5guu79VoAgJGDUXkmXpanjnzZzwQ6WrPrU84vfpHq', '2017-10-04 02:59:02', '2017-10-05 03:50:20', 1),
(3, 3, 'Debbie', 'debbiec@uassist.me', 'users/default.png', '$2y$10$qYlMBCY5neYCsFQWAN5qiO/hYsDKofr.Eqe.fF4P6MbVRH0fopzey', '2RhKIlHW788RJxQaFCZhb7EqrWqy3sQqKsg4RSESlwdY4UNaKQ6i4mUbIjpd', '2017-10-04 20:42:30', '2017-10-05 00:56:18', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `weekvalues`
--

CREATE TABLE `weekvalues` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_value` int(11) DEFAULT NULL,
  `id_team` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `weekvalues`
--

INSERT INTO `weekvalues` (`id`, `id_value`, `id_team`, `date`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '2017-10-06', '2017-10-07 01:08:43', '2017-10-07 01:08:43');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `employee_months`
--
ALTER TABLE `employee_months`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `uamvalues`
--
ALTER TABLE `uamvalues`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `weekvalues`
--
ALTER TABLE `weekvalues`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `employee_months`
--
ALTER TABLE `employee_months`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT de la tabla `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `uamvalues`
--
ALTER TABLE `uamvalues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `weekvalues`
--
ALTER TABLE `weekvalues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    
        Route::get('/', 'HomeController@index');
        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/news/{param}', 'NewsController@index');
        Route::get('/page/{param}', 'PageController@index');
        Route::get('/values/our-values', 'PageController@values');
        Route::get('/uam/our-organization', 'PageController@organization');
        Route::get('/downloads', 'PageController@downloads');
});


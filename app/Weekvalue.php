<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Weekvalue extends Model
{
    protected $table='weekvalues';
    protected $primaryKey ='id';
    public $timestamps = true;
    protected $fillable=['id_value', 'id_team','date'];
    
    public function team()
    {
        return $this->belongsTo('App\Team', 'id_team', 'id');
    }
    public function value()
    {
        return $this->belongsTo('App\Uamvalue', 'id_value', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uamvalue extends Model
{
    //
    protected $table='uamvalues';
    protected $primaryKey ='id';
    public $timestamps = true;
	protected $fillable=['name', 'body'];
}

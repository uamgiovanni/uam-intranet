<?php

namespace App\Http\Controllers;

use App\Uamvalue;
use Illuminate\Http\Request;

class UamvaluesController extends Controller
{
    //
    function index()
    {
        $values = Uamvalue::all();
        return view('values')->with('values',$values)->with('page_label', 'Uassistme values');
    }
}

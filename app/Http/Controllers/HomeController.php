<?php

namespace App\Http\Controllers;

use App\Weekvalue;
use App\EmployeeMonth;
use App\Uamvalue;
use Carbon\Carbon;
use TCG\Voyager\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Post::where('category_id', 1)
                        ->where('status', 'PUBLISHED')
                        ->orderBy('created_at', 'desc')
                        ->get();
        $employees_by_current_month =  EmployeeMonth::whereYear('month', '=', date('Y'))
                                                        ->whereMonth('month', '=', Carbon::now()->subMonth()->month)
                                                        ->get();
        $current_week_uamvalue = Weekvalue::whereBetween('date', [
                                    Carbon::parse('last friday')->startOfDay(),
                                    Carbon::parse('next sunday')->endOfDay(),
                                ])->get();;         
        return view('home')->with('news',$news)
                           ->with('employees_by_current_month',$employees_by_current_month)
                           ->with('current_week_uamvalue',$current_week_uamvalue)
                           ->with('page_label', 'Welcome');
    }
}

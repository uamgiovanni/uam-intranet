<?php

namespace App\Http\Controllers;

use Storage;
use TCG\Voyager\Models\Page;
use App\Uamvalue;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($param)
    {
        $page = Page::where('slug', $param)
                ->first();
        if($page != null)
            return view('news')->with('new',$page)->with('page_label', $page->title);
        else
            return redirect('/');
    }

    public function values()
    {
        $values = Uamvalue::all();
        return view('values')->with('values',$values)->with('page_label', 'UAM Values');
    }
    public function organization()
    {
        return view('organizationchart')->with('page_label', 'Organization chart');
    }

    public function downloads()
    {     
        $files = Storage::allFiles('public/Downloads');
        return view('downloads')->with('page_label', 'Downloads')->with('files', $files);
    }
}

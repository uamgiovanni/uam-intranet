<?php

namespace App\Http\Controllers;

use TCG\Voyager\Models\Post;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($param)
    {
        $post = Post::where('slug', $param)
                ->firstOrFail();
        if($page != null)
            return view('news')->with('new',$post)->with('page_label', $post->title);
        else
            return redirect('/');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeMonth extends Model
{
    protected $table='employee_months';
    protected $primaryKey ='id';
    public $timestamps = true;
    protected $fillable=['user_id', 'month'];
    
    public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }
}

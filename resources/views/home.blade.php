@extends('layouts.app')

@section('content')
        
                <div class="col-12">
                    <h2>News</h2>
                </div>
                @foreach($news as $item)
                    
                    <div class="col-sm-3 news-boxes">
                        <a href="news/{{$item->slug}}"><img class="img-fluid" src="{{Voyager::image($item->image)}}"/>
                        <h5>{{$item->title}}</h5></a>
                    </div>
                    
                @endforeach
                <div class="col-12">
                    <h2>{{\Carbon\Carbon::now()->subMonth()->format('F')}}  employees of the month</h2>
                </div>
                
                @foreach($employees_by_current_month as $employee)
                    <div class="col-sm-2 news-boxes">
                        <img class="img-fluid" src="{{Voyager::image($employee->user->avatar)}}"/>
                        <h5>{{$employee->user->name}}</h5>
                    </div>
                @endforeach
    
                @if(count($current_week_uamvalue) > 0)
                <div class="col-12">
                @php
                    $link = json_decode($current_week_uamvalue[0]->newsletterfile);
                @endphp

                    <h5>This week value is: {{$current_week_uamvalue[0]->value->name}}, <a href="storage/{{$link[0]->download_link}}" target="_blank">download the newsletter </a> thanks to {{$current_week_uamvalue[0]->team->name}}</h5>
                </div>
                @endif  
                    
                                
                
@endsection

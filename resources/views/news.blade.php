@extends('layouts.app')

@section('content')
        <div class="col-12 news-post">
            <p class="new-date" >Published on: {{$new->created_at->diffForHumans()}}</p>
            <img class="img-fluid news-images" src="{{Voyager::image($new->image)}}" />
                <?= str_replace("\/", "/", $new->body)?>
        </div>
                
                
@endsection

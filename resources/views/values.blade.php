@extends('layouts.app')

@section('content')
        <div class="col-12 col-md-8 offset-md-2 news-post">
            <!-- -->
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach($values as $key => $value)
                        <div class="card">
                        <div class="card-header" role="tab" id="heading{{$key}}">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                            {{$value->name}}
                            </a>
                        </h5>
                        </div>

                        <div id="collapse{{$key}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$key}}">
                        <div class="card-block">
                            {{$value->body}}
                        </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            <!-- -->
        </div>
@endsection